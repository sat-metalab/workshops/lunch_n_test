# LivePose

Real-time Pose Estimation for Video Streams

[https://gitlab.com/sat-metalab/livepose](https://gitlab.com/sat-metalab/livepose)

---

## Pose Estimation

**Goal**: To estimate the pose of a person in an image by locating special body
points (**keypoints**).

![](images/livepose/keypoints_and_people.png)

---

## In The Satosphere
<iframe src="images/livepose/openpose_walk_0.mp4" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

---

## LivePose Features

**Accessibility**: Easy to use in many different settings.

---

## LivePose Features

**Accessibility**: Easy to use in many different settings.

* Easy to install on Ubuntu 20.04! Ready to run out-of-the-box.

---

## LivePose Features

Multiple **output** formats available.

* **OSC** (Open Sound Control)
* Use OSC messages to update sound and lighting in an interactive environment based on poses

---

## LivePose Features

Multiple **output** formats available.

* **Websocket**: For controlling web applications with real-time pose data.

<iframe src="https://player.vimeo.com/video/604196712?h=d4ab3f4b2a#&autoplay=1" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

---

## LivePose Features

Multiple Pose Estimation models supported for different use cases.

* **PoseNet**: lightweight and fast, can run on mobile and embedded devices
* **OpenPose**: High accuracy, for use with dedicated GPUs.

---

## LivePose Features

* **Action** and **Gesture** Detection
* Control an interactive environment with gestures like raising arms, jumping, etc.

![](images/livepose/arms_up.png)

